#include <iostream>
#include <algorithm>
#include <ctime>
#define N 100000

void QuickSort(int a, int b, int x[N])
{
	if (a >= b)
		return;
	int m = rand() * rand() % (b - a + 1) + a;
	int k = x[m];
	int l = a - 1;
	int r = b + 1;
	while (1)
	{
		do l++; while (x[l] < k);
		do r--; while (x[r] > k);
		if (l >= r)
			break;
		std::swap(x[l], x[r]);
	}
	r = l;
	l--;
	QuickSort(a, l, x);
	QuickSort(r, b, x);
}

int Search(int x[N], int n, int k)
{
	if (k < x[0])
		return 0;
	if (k == x[0])
		return 1;
	if (k > x[n])
		return 0;

	int a = 1;
	int b = n;
	while (a + 1 < b)
	{
		int c = (a + b) / 2;
		if (k > x[c])
			a = c;
		else
			b = c;
	}
	if (x[b] == k)
		return b;
	else
		return 0;
}

int main()
{
	int a = 1;
	int n = 10000;

	srand(time(0));
	int x[N];
	for (int i = a - 1; i < n; i++)
		x[i] = rand();

	int k = x[9999];
	for (int i = a; i < n - 1; i++)
	    if (x[i] == k)
		    std::cout << "Found " << k << std::endl;

	QuickSort(a, n, x);
	std::cout << "Search " << Search(x, n, k) << std::endl;

	std::cout << "Running time: " << clock() << std::endl;

	return 0;
}
