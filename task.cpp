#include <iostream>
#include <cmath>
const double precision = 0.0001;

double f(double x)
{
	return (x - 10);
}

double Bisection(double f, double a, double eps)
{
    double c = 0;
	while ((b - a) > precision) {
		c = (a + b) / 2;
		if (signbit(f(c)) == signbit(f(b)))
			b = c;
		else
			a = c;
		std::cout << "a: " << a << " b: " << b << std::endl;
	}
	return c;
}

int main()
{
    double a = 0;
	double b = 95;
	
	std::cout << "Root of the equation: " << Bisection(a, b, precision) << std::endl;

	return 0;
}
